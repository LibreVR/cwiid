(define-module (gnu packages cwiid)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
  #:use-module (guix git-download)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pkg-config)
  )

;; create a git mirror for use with guix
(define-public cwiid
  (package
    (name "cwiid")
    (version "0.6.20220219")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/abstrakraft/cwiid")
                    (commit version)))
              (sha256
               (base32
                "TODO"))))              
    (build-system gnu-build-system)
    (arguments '(#:configure-flags '("--without-python")))
   
    (inputs `(
               ("autoconf" ,autoconf)
               ("automake" ,automake)
               ("flex" ,flex)
               ("bison" ,bison)
               ("bluez" ,bluez)
               ("pkg-config" ,pkg-config)
               ("gtk+" ,gtk+-2)
             ))
    (synopsis "The Wiimote driver for GNU/Linux")
    (description "A high-quality speech analysis, manipulation and synthesis system.")
    ;; move to stable gitmirror (codeberg/librevr)
    (home-page "https://github.com/abstrakraft/cwiid")
    (license gpl2+)))

